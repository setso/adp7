package main;

import adp7.ArrayKnot;
import adp7.BinaryTreeArray;
import adp7.BinaryTreeContainer;

public class TestFrame_Cont {
	
	public static void main(String[] args) {
		BinaryTreeContainer<Integer> btc = new BinaryTreeContainer<Integer>();
		
//		btc.insertVertex(2);
//		btc.insertVertex(8);
//		btc.insertVertex(2);
//		btc.insertVertex(5);
//		btc.insertVertex(3);
//		btc.insertVertex(92);
//		btc.insertVertex(100);
//		btc.insertVertex(414);
//		btc.insertVertex(424);
//		btc.insertVertex(0);
		
//		for (int i = 10; i > 0; i--) {
//		btc.insertVertex((int) (Math.random()*1000));
//	}
//		
////		btc.insertVertex("A");
////		btc.insertVertex("C");
////		btc.insertVertex("B");
//		
//		System.out.println("Baum - Muster: Knoten : linker Sohn + rechter Sohn");
//		btc.printTree();
//		
//		System.out.println();
//		
//		btc.printVertexInorder();
//		btc.printVertexPostorder();
//		btc.printVertexPreorder();
		
		BinaryTreeArray<ArrayKnot<Integer>> bta = new BinaryTreeArray<ArrayKnot<Integer>>(50);
		
		System.out.println("elements " + bta.count(0));
		bta.insertVertex(new ArrayKnot<>(19, new Integer(8)));
		bta.insertVertex(new ArrayKnot<>(8, new Integer(8)));
		bta.insertVertex(new ArrayKnot<>(12, new Integer(8)));
		bta.insertVertex(new ArrayKnot<>(5, new Integer(8)));
		bta.insertVertex(new ArrayKnot<>(13, new Integer(8)));
//		System.out.println(bta.insertVertex(new ArrayKnot<>(18, new Integer(8))));
		bta.insertVertex(new ArrayKnot<>(22, new Integer(8)));
		bta.insertVertex(new ArrayKnot<>(20, new Integer(8)));
		bta.insertVertex(new ArrayKnot<>(24, new Integer(8)));
		
		System.out.println("elements " + bta.count(0));
		
		bta.printVertexInorder();
		bta.printVertexPostorder();
		bta.printVertexPreorder();
		
	}

	
}
