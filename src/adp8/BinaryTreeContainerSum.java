package adp8;

import adp7.SuchbaumInterface;

public class BinaryTreeContainerSum implements SuchbaumInterface<Integer> {

	private ContainerSum root; //Wurzel des Baumes
	
	
	/**
	 * Methode zum aktualisieren der summierten Min Werte
	 */
	
	private void updateSumMin(ContainerSum c, ContainerSum n)
	{
		if(c.getData() <= n.getData())
		{
			n.addToSmallerNodes(c.getData());
		}
		if(n.getData() <= c.getData())
		{
			c.addToSmallerNodes(n.getData());
		}
		
		if(c.getLeftSon() != null)
		{
			updateSumMin(c.getLeftSon(), n);
		}
		
		if(c.getRightSon() != null)
		{
			updateSumMin(c.getRightSon(), n);
		}
	}
	
	/**
	 * Methode, welche die Summe der Elemente im Baum
	 * zurückgibt, die größer gleich min und kleiner gleich max sind
	 */
	public int calculateSum(int min, int max)
	{
		int sum = 0;
		ContainerSum minCont;
		ContainerSum maxCont;
		
		minCont = findMin(root, min, max);
		maxCont = findMax(root, min, max);
		
//		//*******************************
//		System.out.println();
//		if(maxCont != null) {
//			for(int i = 0; i < maxCont.getSumMin().size(); i++) {
//				System.out.println("maxCont: " + maxCont.getSumMin().get(i));
//			}
//			System.out.println("maxCont data: " + maxCont.getData());
//		}
//		if(minCont != null) {
//			System.out.println("minCont data: " + minCont.getData());
//		}
//		//******************************
		
		if(minCont != null && maxCont != null) {
			sum = sumOf(minCont, maxCont);
		}
		
		return sum;
	}
	
	/**
	 * Aufsummieren der Werte der einzelnen ArrayList von min bis max
	 */
	private int sumOf(ContainerSum min, ContainerSum max)
	{
		int sum = 0;
		for(int i = 0; i <max.getSmallerNodes().size(); i++)
		{
			if(max.getSmallerNodes().get(i) >= min.getData())
			{
				sum += max.getSmallerNodes().get(i);
			}
		}
		sum+= max.getData(); //TESTEN!!! TODO
		return sum;
	}
	
	/**
	 * Minimum finden
	 */
	private ContainerSum findMin(ContainerSum c, int min, int max)
	{
		ContainerSum minCont = null;
		ContainerSum minContL = null;
		ContainerSum minContR = null;
		
		if(c.getLeftSon() != null)
		{
			minContL = findMin(c.getLeftSon(),min,max);
		}
		if(c.getRightSon() != null)
		{
			minContR = findMin(c.getRightSon(),min,max);
		}
		
		if(minContL == null)
		{
			if(minContR == null)
			{
				if(c.getData() >= min && c.getData() <= max)
				{
					minCont = c;
				}
			}else{
				if(minContR.getData() >= min && minContR.getData() <= max) {
					minCont = minContR;	
			}
		}
		}else if(minContR == null)
		{
			if(minContL.getData() >= min && minContL.getData() <= max) {
				minCont = minContL;
		}
		}else{
			if(minContL.getData() <= minContR.getData()) {
				if(minContL.getData() >= min && minContL.getData() <= max) {
					minCont = minContL;
				} else if(minContR.getData() >= min && minContR.getData() <= max) {
					minCont = minContR;
				}
			}else {
				if(minContR.getData() >= min && minContR.getData() <= max) {
					minCont = minContR;
				} else if(minContL.getData() >= min && minContL.getData() <= max) {
					minCont = minContL;
			}
		}
	}
		if(minCont != null) {
			if(minCont.getData() > c.getData()) {
				if(c.getData() >= min && c.getData() <= max) {
					minCont = c;
				}
			}
		}
		
		return minCont;
	}
	
	/**
	 * Methode zum finden des ContainerSums max.
	 * @param min
	 * @param max
	 * @return
	 */
	private ContainerSum findMax(ContainerSum c, int min, int max) {
		ContainerSum maxCont = null;
		ContainerSum maxContL = null;
		ContainerSum maxContR = null;
		
		if(c.getLeftSon() != null) {
			maxContL = findMax(c.getLeftSon(), min, max);
		}
		if(c.getRightSon() != null) {
			maxContR = findMax(c.getRightSon(), min, max);
		}
		
		if(maxContL == null) {
			if(maxContR == null) {
				if(c.getData() >= min && c.getData() <= max) {
					maxCont = c;
				}
			} else {
				if(maxContR.getData() >= min && maxContR.getData() <= max) {
					maxCont = maxContR;
				}
			}
		} else if(maxContR == null) {
			if(maxContL.getData() >= min && maxContL.getData() <= max) {
				maxCont = maxContL;
			}
		} else {
			if(maxContL.getData() >= maxContR.getData()) {
				if(maxContL.getData() >= min && maxContL.getData() <= max) {
					maxCont = maxContL;
				} else if(maxContR.getData() >= min && maxContR.getData() <= max) {
					maxCont = maxContR;
				}
			} else {
				if(maxContR.getData() >= min && maxContR.getData() <= max) {
					maxCont = maxContR;
				} else if(maxContL.getData() >= min && maxContL.getData() <= max) {
					maxCont = maxContL;
				}
			}
		}
		
		if(maxCont != null) {
			if(maxCont.getData() < c.getData()) {
				if(c.getData() >= min && c.getData() <= max) {
					maxCont = c;
				}
			}
		}
		
		return maxCont;
	}
	
	
	@Override
	public boolean insertVertex(Integer elem) {
		if(elem instanceof Comparable<?>){
			ContainerSum n = new ContainerSum(elem);
			if(root == null) {
				root = n;
			} else {
				updateSumMin(root, n);
				insertPos(root, n);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Sucht eine Stelle zum einfügen des Vertex n.
	 * @param c
	 * @param n
	 */
	private void insertPos(ContainerSum c, ContainerSum n) {
		if(c.getData().compareTo(n.getData()) < 0) {
			if(c.getRightSon() == null) {
				c.setRightSon(n);
				n.setFather(c);
			} else {
				insertPos(c.getRightSon(), n);
			}
		} else {
			if(c.getLeftSon() == null) {
				c.setLeftSon(n);
				n.setFather(c);
			} else {
				insertPos(c.getLeftSon(), n);
			}
		}
	}
	
	@Override
	public void printVertexPreorder() {
		if(root != null) {
			System.out.print("Preorder:  ");
			System.out.print(" " + root.getData());
			if(root.getLeftSon() != null) {
				printVertexPre(root.getLeftSon());
			}
			if(root.getRightSon() != null) {
				printVertexPre(root.getRightSon());
			}
			System.out.println();
		}	
	}
	
	/**
	 * Rekursive Methode zur Ausgabe in Hauptreihenfolge ab einem bestimmten Knoten.
	 * @param v
	 */
	private void printVertexPre(ContainerSum v) {
		System.out.print(" " + v.getData());
		if(v.getLeftSon() != null) {
			printVertexPre(v.getLeftSon());
		}
		if(v.getRightSon() != null) {
			printVertexPre(v.getRightSon());
		}
	}

	@Override
	public void printVertexPostorder() {
		if(root != null) {
			System.out.print("Postorder: ");
			if(root.getLeftSon() != null) {
				printVertexPost(root.getLeftSon());
			}
			if(root.getRightSon() != null) {
				printVertexPost(root.getRightSon());
			}
			System.out.print(" " + root.getData());
			System.out.println();
		}
		
	}
	
	/**
	 * Rekursive Methode zur Ausgabe in Nebenreihenfolge ab einem bestimmten Knoten.
	 * @param v
	 */
	private void printVertexPost(ContainerSum v) {
		if(v.getLeftSon() != null) {
			printVertexPost(v.getLeftSon());
		}
		if(v.getRightSon() != null) {
			printVertexPost(v.getRightSon());
		}
		System.out.print(" " + v.getData());
	}

	@Override
	public void printVertexInorder() {
		if(root != null) {
			System.out.print("Inorder:   ");
			if(root.getLeftSon() != null) {
				printVertexIn(root.getLeftSon());
			}
			System.out.print(" " + root.getData());
			if(root.getRightSon() != null) {
				printVertexIn(root.getRightSon());
			}
			System.out.println();
		}
	}


	/**
	 * Rekursive Methode zur symmetrischen Ausgabe ab einem bestimmten Knoten.
	 * @param v
	 */
	private void printVertexIn(ContainerSum v) {
		if(v.getLeftSon() != null) {
			printVertexIn(v.getLeftSon());
		}
		System.out.print(" " + v.getData());
		if(v.getRightSon() != null) {
			printVertexIn(v.getRightSon());
		}
	}
	
	/**
	 * Gibt den gesamten Baum nach folgendem Muster aus: "Knoten : linker Sohn + rechter Sohn".
	 */
	public void printTree() {
		System.out.println("Tree:");
		printVertex(root);
	}
	
	/**
	 * Rekursive Methode zur Ausgabe eines Knotens nach folgendem Muster: "Knoten : linker Sohn + rechter Sohn".
	 * @param v
	 */
	private void printVertex(ContainerSum v) {
		
		if (v.getData() == null) {
			System.out.print("x");
		} else {
			System.out.print(v.getData());
		}
		System.out.print(" : ");
		if (v.getLeftSon() == null) {
			System.out.print("x");
		} else {
			System.out.print(v.getLeftSon().getData());
		}
		System.out.print(" + ");
		if (v.getRightSon() == null) {
			System.out.print("x");
		} else {
			System.out.print(v.getRightSon().getData());
		}
		
		System.out.println();
		
		if (v.getLeftSon() != null) {
			printVertex(v.getLeftSon());
		}
		if (v.getRightSon() != null) {
			printVertex(v.getRightSon());
		}
	}
	

}
