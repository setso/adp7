/**
 * @author bud, setso
 * @version 1
 * @date 19.11.15
 */
package adp7;

public class BinaryTreeArray<T> implements SuchbaumInterface<T> {

    ArrayKnot[] binaryTreeArray;

    /**
     * Constructor is initialized with the maximum array size.
     *
     * Constructor
     * @param size    maximum size as int
     */
    public BinaryTreeArray(int size) {
        binaryTreeArray=new ArrayKnot[size];
    }

    /**
     * Prints the vertex keys in the order father, child, child to console
     */
    @Override
    public void printVertexPreorder() {
        System.out.println("preorder");
        System.out.println(printVertexPreorder(0));
    }

    /**
     * Creates the string for a given root in preorder
     *
     * @param    root    the root of the (sub)tree
     * @return the string for a given root in preorder
     */
    public String printVertexPreorder(int root) {
        String stringChild1="";
        if(binaryTreeArray[(root * 2) + 1] != null) {
            stringChild1=printVertexPreorder((root * 2) + 1);
        }

        String sFather="";
        if(binaryTreeArray[root] != null) {
            sFather=String.valueOf(binaryTreeArray[root].getKey());
        }

        String sChild2="";
        if(binaryTreeArray[(root * 2) + 2] != null) {
            sChild2=printVertexPreorder((root * 2) + 2);
        }

        return (sFather + " " + stringChild1 + " " + sChild2 + " ");
    }

    /**
     * Prints the vertex keys in the order child, child, father to console
     */
    @Override
    public void printVertexPostorder() {
        System.out.println("postorder");
        System.out.println(printVertexPostorder(0));
    }

    /**
     * Creates the string for a given root in postorder
     *
     * @param    root    the root of the (sub)tree
     * @return the string for a given root in postorder
     */
    public String printVertexPostorder(int root) {
        String stringChild1="";
        if(binaryTreeArray[(root * 2) + 1] != null) {
            stringChild1=printVertexPostorder((root * 2) + 1);
        }

        String sFather="";
        if(binaryTreeArray[root] != null) {
            sFather=String.valueOf(binaryTreeArray[root].getKey());
        }

        String sChild2="";
        if(binaryTreeArray[(root * 2) + 2] != null) {
            sChild2=printVertexPostorder((root * 2) + 2);
        }

        return (stringChild1 + " " + sChild2 + " " + sFather + " ");
    }

    /**
     * Prints the vertex keys in the order child, father, child to console
     */
    @Override
    public void printVertexInorder() {
        System.out.println("inorder");
        System.out.println(printVertexInorder(0));
    }

    /**
     * Creates the string for a given root in inorder
     *
     * @param    root    the root of the (sub)tree
     * @return the string for a given root in inpostorder
     */
    public String printVertexInorder(int root) {
        String stringChild1="";
        if(binaryTreeArray[(root * 2) + 1] != null) {
            stringChild1=printVertexInorder((root * 2) + 1);
        }

        String sFather="";
        if(binaryTreeArray[root] != null) {
            sFather=String.valueOf(binaryTreeArray[root].getKey());
        }

        String sChild2="";
        if(binaryTreeArray[(root * 2) + 2] != null) {
            sChild2=printVertexInorder((root * 2) + 2);
        }

        return (stringChild1 + " " + sFather + " " + sChild2 + " ");
    }


    /**
     * Inserts a given vertex in path specified by the given root
     *
     * @param elem        object of the class ArrayKnot with unique key
     * @param pathRoot    the position of the root vertex in the array
     * @return true if insert operation successful
     */
    public boolean insertVertexInPath(T elem, int pathRoot) {

        if(binaryTreeArray[pathRoot] == null) {
            binaryTreeArray[pathRoot]=(ArrayKnot) elem;
            return true;
        } else if(binaryTreeArray[pathRoot].getKey() > ((ArrayKnot) elem).getKey()) {
            return insertVertexInPath(elem, (pathRoot * 2) + 1);
        } else if(binaryTreeArray[pathRoot].getKey() < ((ArrayKnot) elem).getKey()) {
            return insertVertexInPath(elem, (pathRoot * 2) + 2);
        }
        return false;

    }

    /**
     * Inserts the given vertex in the BinaryTreeArray by comparing the objects key
     *
     * @param elem        object of the class ArrayKnot with unique key
     * @return boolean    true if insert operation successful
     */
    @Override
    public boolean insertVertex(T elem) {
        return this.insertVertexInPath(elem, 0);
    }

    /**
     * Counts the elements below and including a given root
     *
     * @param father the root element
     * @return
     */
    public int count(int father) {
        if(binaryTreeArray[father] == null) return 0;
        else {
            int count=1;
            count+=count((father * 2) + 1);
            count+=count((father * 2) + 2);
            return count;
        }
    }

    /**
     * Returns the reference for the active Array
     * @return ArrayKnot[]
     */
    public ArrayKnot[] getBinaryTreeArray() {
        return binaryTreeArray;
    }

//    public void updateCombinedVertexValue() {
//        for (int x = binaryTreeArray.length-1; x>=0; x--){
//            if (binaryTreeArray[x]!=null){
//
//                if (((x * 2) + 1)>binaryTreeArray.length) {
//                    binaryTreeArray[x].setSubVertexValue((int) binaryTreeArray[x].getValue());
//                }
//                else if (binaryTreeArray[((x * 2) + 1)]==null&&binaryTreeArray[((x * 2) + 2)]==null){
//                    binaryTreeArray[x].setSubVertexValue((int) binaryTreeArray[x].getValue());
//                }
//                else{
//                    int child1=0;
//                    int child2=0;
//                    if (binaryTreeArray[(x * 2) + 1]!=null){child1=binaryTreeArray[(x * 2) + 1].getSubVertexValue();}
//                    if (binaryTreeArray[(x * 2) + 2]!=null){child2=binaryTreeArray[(x * 2) + 2].getSubVertexValue();}
//                    binaryTreeArray[x].setSubVertexValue(child1+child2+(int)binaryTreeArray[x].getValue());
//                }
//            }
//
//        }
//    }

    /**
     * Calculates the sum of all elements between the given keys
     *
     * The internal sum of the elements has to be up to date.
     * Update with method call updateSumLarger()
     *
     * @param int lower boundary
     * @param int upper boundary
     * @return int Sum between the given boundaries
     */
    public int getSumBetween(int a, int b) {
        ArrayKnot smallKnot = findSmallestKeyLargerOrEqual(a, 0);
        ArrayKnot largeKnot = findLargestKeySmallerOrEqual(b, 0);

        return smallKnot.getSumLarger()-largeKnot.getSumLarger()+(int)smallKnot.getValue();
        
    }

    /**
     * Searches for the smallest key larger or equal to the given boundary
     *
     * @param key lower boundary
     * @param root father element index
     * @return ArrayKnot
     */
    public ArrayKnot findSmallestKeyLargerOrEqual(int key, int root) {
        ArrayKnot knot=new ArrayKnot<>(666, new Integer(666));
        boolean knotPossibile=true;
        int sIndex=0;
        while(true) {
            if(binaryTreeArray[sIndex].getKey() == key) {
                knot=binaryTreeArray[sIndex];
                break;
            } else if(binaryTreeArray[sIndex].getKey() > key) {
                if(binaryTreeArray[sIndex].getKey() < knot.getKey() && binaryTreeArray[sIndex].getKey() >= key) {knot=binaryTreeArray[sIndex];}
                if(binaryTreeArray[(sIndex * 2) + 1] == null) {break;}
                sIndex=(sIndex * 2) + 1;
            } else if(binaryTreeArray[sIndex].getKey() < key) {
                if(binaryTreeArray[sIndex].getKey() < knot.getKey() && binaryTreeArray[sIndex].getKey() >= key) {
                    knot=binaryTreeArray[sIndex];
                }
                if(binaryTreeArray[(sIndex * 2) + 2] == null) {break;}
                sIndex=(sIndex * 2) + 2;
            }
        }
        return knot;
    }

    /**
     *  Searches for the largest key smaller or equal to the given boundary
     *
     * @param key upper boundary
     * @param root father element index
     * @return ArrayKnot
     */
    public ArrayKnot findLargestKeySmallerOrEqual(int key, int root) {
        ArrayKnot knot=new ArrayKnot<>(0, new Integer(0));
        int sIndex=0;
        while(true) {
            if(binaryTreeArray[sIndex].getKey() == key) {
                knot=binaryTreeArray[sIndex];
                break;
            } else if(binaryTreeArray[sIndex].getKey() > key) {
                if(binaryTreeArray[sIndex].getKey() > knot.getKey()  && binaryTreeArray[sIndex].getKey() <= key) {knot=binaryTreeArray[sIndex];}
                if(binaryTreeArray[(sIndex * 2) + 1] == null) {break;}
                sIndex=(sIndex * 2) + 1;
            } else if(binaryTreeArray[sIndex].getKey() < key) {
                if(binaryTreeArray[sIndex].getKey() > knot.getKey() && binaryTreeArray[sIndex].getKey() <= key) {
                    knot=binaryTreeArray[sIndex];
                }
                if(binaryTreeArray[(sIndex * 2) + 2] == null) {break;}
                sIndex=(sIndex * 2) + 2;
            }
        }
        return knot;
    }

    /**
     * Updates the internal sum to the largest element.
     * All elements are updated.
     */
    public void updateSumLarger(){
        for (int x=0; x < binaryTreeArray.length; x++){
            if(binaryTreeArray[x]!=null){
                binaryTreeArray[x].setSumLarger(calculateLargerSum(x));
            }
        }
    }

    /**
     * Calculates the sum of element values from a given element to the largest element
     * @param int index of the start element
     * @return int sum
     */
    private int calculateLargerSum(int i) {
        int val = (int) binaryTreeArray[i].getValue();
        int sum = 0;
        for (int x=0;x < binaryTreeArray.length; x++){
            if(binaryTreeArray[x]!=null&&(int)binaryTreeArray[x].getValue()>val){
                sum = sum + (int) binaryTreeArray[x].getValue();
            }
        }
        return sum;
    }

}
