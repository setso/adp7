package adp8.main;


import adp8.BinaryTreeContainerSum;

public class TestFrame_ContSum {
	
	public static void main(String[] args) {
		
BinaryTreeContainerSum btc = new BinaryTreeContainerSum();		 
		 int sum;
		 int min;
		 int max;
		 
			System.out.println("-----------------Veranschaulichung-----------------");
			System.out.println();
			
			for (int i = 10; i > 0; i--) {
				btc.insertVertex((int) (Math.random()*1000));
			}
			btc.printTree();
			System.out.println();
			btc.printVertexInorder();
			
			min = 600;
			max = 700;
			
			sum = btc.calculateSum(min, max);	
			System.out.println();
			System.out.println("Sum from " + min + " to " + max + ": " + sum);
			
			System.out.println(); 
	}


}
