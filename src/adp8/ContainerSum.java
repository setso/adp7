/**
 * ADP8 Erweiterung, Container für binären Suchbaum als Integer.
 */
package adp8;

import java.util.ArrayList;

public class ContainerSum {
	
	/**
	 * Inhalt des Containers
	 */
	private int data;
	
	/**
	 * Liste für die Summe der kleineren Container
	 */
	private ArrayList<Integer> smallerNodes;
	
	/**
	 * Vater-Knoten vom Container
	 */
	private ContainerSum father;
	
	/**
	 * Linker Sohn des Container
	 */
	private ContainerSum leftSon;
	
	/**
	 * Rechter Sohn des Container
	 */
	private ContainerSum rightSon;
	
	public ContainerSum(int inhalt) {
		this.data = inhalt;
		this.smallerNodes = new ArrayList<Integer>();
	}
	
	/**
	 * Getter für data
	 * @return inhalt
	 */
	public Integer getData()
	{
		return data;
	}
	
	/**
	 * 
	 * @return kleine Knoten
	 */
	public ArrayList<Integer> getSmallerNodes()
	{
		return smallerNodes;
	}
	
	/**
	 * Addiert Wert zu Summen kleiner
	 */
	public void addToSmallerNodes(int s)
	{
		this.smallerNodes.add(s);
	}
	
	/**
	 * Getter für Vater Container
	 * @return vater
	 */
	public ContainerSum getFather()
	{
		return father;
	}
	
	/**
	 * Getter für linken Sohn des Containers
	 * @return
	 */
	public ContainerSum getLeftSon()
	{
		return leftSon;
	}
	
	/**
	 * Getter für rechten Sohn des Containers
	 * @return
	 */
	public ContainerSum getRightSon()
	{
		return rightSon;
	}
	
	/**
	 * Setter für Vater des Container
	 * @param vater
	 */
	public void setFather(ContainerSum vater)
	{
		this.father = vater;
	}
	
	/**
	 * Setter für linken Sohn des Container
	 * @param linkerSohn
	 */
	public void setLeftSon(ContainerSum linkerSohn)
	{
		this.leftSon = linkerSohn;
	}
	
	/**
	 * Setter für rechten Sohn des Container
	 * @param rechterSohn
	 */
	public void setRightSon(ContainerSum rechterSohn)
	{
		this.rightSon = rechterSohn;
	}

}
