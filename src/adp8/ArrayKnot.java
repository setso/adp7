package adp7;

public class ArrayKnot<T extends Comparable<T>> {

    private int key;
    private T value;
    private int subVertexValue;
    private int sumLarger;


    public ArrayKnot(int _key, T _value) {
        key=_key;
        value=_value;
        subVertexValue=0;
    }

    public int getKey() {
        return key;
    }

    public T getValue() {
        return value;
    }

    public String toString() {
        return "(" + key + ", " + value + ")";
    }

    public int compareTo(T compKnot) {
        // -1 smaller, 0 same, +1 greater
        if(this.key < ((ArrayKnot<?>) compKnot).getKey()) {
            return -1;
        } else if(this.key > ((ArrayKnot<?>) compKnot).getKey()) {
            return 1;
        }
        if(this.key == ((ArrayKnot<?>) compKnot).getKey()) {
            return 0;
        }

        return 99;
    }

    public void setSubVertexValue(int subVal){
        subVertexValue=subVal;
    }


    public int getSubVertexValue() {
        return subVertexValue;
    }

    public void setSumLarger(int sum){
        sumLarger=sum;
    }

    public int getSumLarger() {
        return sumLarger;
    }
}
